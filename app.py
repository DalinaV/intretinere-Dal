from crypt import methods
from flask import Flask, render_template, request, redirect, url_for, session
from os import environ
from flask_mysqldb import MySQL
import MySQLdb.cursors
import re
from datetime import datetime


app = Flask(__name__)
APP_KEY = environ.get('APP_KEY')
app.config['SECRET_KEY'] = 'some random string'

# Enter your database connection details below
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = '92.Ioana'
app.config['MYSQL_DB'] = 'pythonlogin'

# Intialize MySQL
mysql = MySQL(app)


# index route
@app.route('/')
def home_page():
    # Check if user is loggedin
    if 'loggedin' in session:
        # User is loggedin show them the home page
        return render_template('index.html', username=session['username'])
    # User is not loggedin redirect to login page
    return redirect(url_for('conectare_page'))

@app.route('/conectare', methods=['GET', 'POST'])
def conectare_page():
    # Output message if something goes wrong...
    msg = ''
    # Check if "username" and "password" POST requests exist (user submitted form)
    if request.method == 'POST' and 'username' in request.form and 'password' in request.form:
        # Create variables for easy access
        username = request.form['username']
        password = request.form['password']
         # Check if account exists using MySQL(sql querry)
        cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute('SELECT * FROM accounts WHERE username = %s AND password = %s', (username, password,))
        # Fetch one record and return result
        account = cursor.fetchone()
         # If account exists in accounts table in out database
        if account:
            # Create session data, we can access this data in other routes
            session['loggedin'] = True
            session['accounts_id'] = account['accounts_id']
            session['username'] = account['username']
            session['nr_apartament'] = account['nr_apartament']
            # Redirect to home page
            return redirect(url_for('home_page'))
        else:
            # Account doesnt exist or username/password incorrect
            msg = 'Incorrect username/password!'
    return render_template('conectare.html', msg=msg)


# http://localhost:5000/python/logout - this will be the logout page
@app.route('/logout')
def logout():
    # Remove session data, this will log the user out
   session.pop('loggedin', None)
   session.pop('accounts_id', None)
   session.pop('username', None)
   # Redirect to login page
   return redirect(url_for('conectare_page'))





@app.route('/inregistrare', methods=['GET', 'POST'])
def inregistrare_page():
    #Output message if something goes wrong...
    msg = ''
     # Check if "username", "password" and "email" POST requests exist (user submitted form)
    if request.method == 'POST' and 'username' in request.form and 'password' in request.form and 'email' in request.form and 'nr_apartament' in request.form:
        # Create variables for easy access
        username = request.form['username']
        password = request.form['password']
        email = request.form['email']
        nr_apartament = request.form['nr_apartament']
         # Check if account exists using MySQL
        cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute('SELECT * FROM accounts WHERE username = %s', (username,))
        account = cursor.fetchone()
        # If account exists show error and validation checks
        if account:
            msg = 'Account already exists!'
        elif not re.match(r'[^@]+@[^@]+\.[^@]+', email):
            msg = 'Invalid email address!'
        elif not re.match(r'[A-Za-z0-9]+', username):
            msg = 'Username must contain only characters and numbers!'
        elif not username or not password or not email or not nr_apartament:
            msg = 'Please fill out the form!'
        else:
            # Account doesnt exists and the form data is valid, now insert new account into accounts table
            cursor.execute('INSERT INTO accounts VALUES (NULL, %s, %s, %s, %s)', (username, password, email, nr_apartament,))
            mysql.connection.commit()
            return redirect(url_for('conectare_page'))
    elif request.method == 'POST':
        # Form is empty... (no POST data)
        msg = 'Please fill out the form!'
    # Show registration form with message (if any)
    return render_template('inregistrare.html', msg=msg)



#@app.route('/test_key')
#def index():
#   return f'APP_KEY = {environ.get("APP_KEY")}'


# 404 error
@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404



#edit informations page
@app.route('/edit', methods=['GET', 'POST'])
def edit():
    msg=''
    if request.method == 'POST' and 'nr_apartament' in request.form and 'luna_cheltuieli' in request.form and 'nr_persoane' in request.form and 'index_apa_MC_vechi' in request.form and 'index_apa_MC_nou' in request.form and data_comunicarii in request.form:
        nr_apartament = request.form['nr_apartament']
        luna_cheltuieli = request.form['luna_cheltuieli']
        nr_persoane = request.form['nr_persoane']
        index_apa_MC_vechi = request.form['index_apa_MC_vechi']
        index_apa_MC_nou = request.form['index_apa_MC_nou']
        data_comunicarii - request.form['data_comunicarii']
        cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute('SELECT * FROM informations WHERE luna_cheltuieli = %s and nr_apartament = %s', (luna_cheltuieli, nr_apartament,))
        account = cursor.fetchone()
        if account:
            cursor.execute ("""Update informations set nr_apartament = %s, luna_cheltuieli = %s, nr_persoane= %s, index_apa_MC_vechi = %s, index_apa_MC_nou = %s, data_comunicarii = %s where luna_cheltuieli = %s and nr_apartament = %s""", (nr_apartament, luna_cheltuieli, nr_persoane, index_apa_MC_vechi, index_apa_MC_nou, data_comunicarii, luna_cheltuieli, nr_apartament,))
            mysql.connection.commit()
            return redirect(url_for('profile'))
        else:
            # Account does exists and the form data is valid, now insert new account into accounts table
            cursor.execute('INSERT INTO informations VALUES (NULL, %s, %s, %s, %s, %s, %s)', (nr_apartament, luna_cheltuieli, nr_persoane, index_apa_MC_vechi, index_apa_MC_nou, data_comunicarii,))
            mysql.connection.commit()
            return redirect(url_for('profile'))
    elif request.method == 'POST':
             # Form is empty... (no POST data)
        msg = 'Please fill out the form!'
    else:
        msg = "eroare"
    return render_template('edit.html',msg=msg )





#profile page with all informations inserted or updated
@app.route('/profile', methods=['GET', 'POST'])
def profile():
    account=[]
    msg=''
    # Check if user is loggedin
    if 'loggedin' in session:
        # We need all the account info for the user so we can display it on the profile page
        cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute("""SELECT informations.luna_cheltuieli, informations.nr_apartament, data_comunicarii, nr_persoane, pret_general_MC, pret_general_gunoi, pret_general_canal, pret_general_curatenie, index_apa_MC_vechi, index_apa_MC_nou, total_MC, cost_apartament_apa, cost_apartament_canal, total_cost_apartament_Canal+Apa, cost_apartament_gunoi, cost_apartament_curatenie, fond_rulment, alte_cheltuieli, diverse, observatii, cost_total_apartament  FROM accounts inner join informations on accounts.nr_apartament = informations.nr_apartament inner join administrator_page on informations.luna_cheltuieli = administrator_page.luna_cheltuieli WHERE accounts.accounts_id = %s""", (session['accounts_id'],))
        if cursor:
            for i in cursor:
                account = cursor.fetchall()
                return render_template('profile.html', account = account)
# User is not loggedin redirect to login page
        else:
            msg = "eroare"
        return render_template('edit.html',msg=msg )






@app.route('/administrator_page', methods=['GET', 'POST'])
def administrator_page():
    msg=''
    cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
    cursor.execute('SELECT * FROM administrator_page ORDER BY administrator_page_id')
    newaccount = cursor.fetchall()
#    return render_template('administrator_page.html', newaccount=newaccount)
        
    if request.method == 'POST'  and 'luna_cheltuieli' in request.form and 'pret_general_MC' in request.form and 'pret_general_gunoi' in request.form and 'pret_general_canal' in request.form and 'pret_general_curatenie' in request.form and 'fond_rulment' in request.form and 'alte_cheltuieli' in request.form and 'diverse' in request.form and 'observatii' in request.form:
        luna_cheltuieli = request.form['luna_cheltuieli']
        pret_general_MC = request.form['pret_general_MC']
        pret_general_gunoi = request.form['pret_general_gunoi']
        pret_general_canal = request.form['pret_general_canal']
        pret_general_curatenie = request.form['pret_general_curatenie']
        fond_rulment = request.form['fond_rulment']
        alte_cheltuieli = request.form['alte_cheltuieli']
        diverse = request.form['diverse']
        observatii = request.form['observatii']
        cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
#        cursor.execute('SELECT * FROM administrator_page')
#        if cursor:
#          for i in cursor:
#               newaccount = cursor.fetchall()
#               return render_template('administrator_page.html', newaccount=newaccount)
        cursor.execute('SELECT * FROM administrator_page WHERE luna_cheltuieli = %s', (luna_cheltuieli,))
        account = cursor.fetchone()
        if  account:
            # Account does exists and the form data is valid, now insert new account into accounts table
            cursor.execute ("""Update administrator_page set luna_cheltuieli = %s, pret_general_MC = %s, pret_general_gunoi = %s, pret_general_canal = %s, pret_general_curatenie = %s, fond_rulment = %s, alte_cheltuieli= %s, diverse= %s, observatii = %s where luna_cheltuieli = %s""", (luna_cheltuieli, pret_general_MC, pret_general_gunoi, pret_general_canal, pret_general_curatenie, fond_rulment, alte_cheltuieli, diverse, observatii, luna_cheltuieli,))
            cursor.execute('SELECT * FROM administrator_page ORDER BY administrator_page_id')
            newaccount = cursor.fetchall()
            mysql.connection.commit()
            return render_template('administrator_page.html', newaccount=newaccount)
        else:
            cursor.execute('INSERT INTO administrator_page VALUES (NULL, %s, %s, %s, %s, %s, %s, %s, %s, %s)', (luna_cheltuieli, pret_general_MC, pret_general_gunoi, pret_general_canal, pret_general_curatenie, fond_rulment, alte_cheltuieli, diverse, observatii,))
            cursor.execute('SELECT * FROM administrator_page ORDER BY administrator_page_id')
            newaccount = cursor.fetchall()
            mysql.connection.commit()
            return render_template('administrator_page.html', newaccount=newaccount)
    elif request.method == 'POST':
        # Form is empty... (no POST data)
        msg = 'Please fill out the form!'
    msg ='doesnt work'
    return render_template('administrator_page.html', newaccount=newaccount)


if __name__ == '__main__':
    app.run()
